package writer

import (
	"io"

	"gitlab.com/cloudcreds.io/cloudcreds/pkg/credentials"
)

//CredentialWriter writes credentials to the output
type CredentialWriter struct {
	out   io.Writer
	input map[string]interface{}
}

//WriteCredential writes a credential to the writer
func (w *CredentialWriter) WriteCredential(c credentials.Credential) error {
	creds := c.GetCredentials(w.input)
	_, err := w.out.Write([]byte(creds))
	if err != nil {
		return err
	}
	return nil

}
