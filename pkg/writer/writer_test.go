package writer

import (
	"bytes"
	"testing"
)

type MockCredential struct{}

func (m MockCredential) GetCredentials(input map[string]interface{}) string {
	return "Mock"
}

func TestWriteCredentials(t *testing.T) {
	mc := MockCredential{}
	var b bytes.Buffer
	cw := CredentialWriter{out: &b}
	err := cw.WriteCredential(mc)
	if err != nil {
		t.Error(err)
	}
	output := b.String()
	if output != "Mock" {
		t.Errorf("%s!=Mock", output)
	}

}
