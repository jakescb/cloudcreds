package credentials

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path"

	"gitlab.com/cloudcreds.io/cloudcreds/pkg/file"
)

//GetInstalledPlugins gets a list of files in the plugin directory
func GetInstalledPlugins(pluginLocation string) []string {

	files := file.WalkFiles(pluginLocation)
	return files
}

//DownloadPlugin effeciently downloads a plugin
func DownloadPlugin(filePath string, pluginLocation string) error {

	// Get the data
	resp, err := http.Get(filePath)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	fileLocation := fmt.Sprintf("%s/%s", pluginLocation, path.Base(filePath))

	// Create the file
	out, err := os.Create(fileLocation)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err

}
