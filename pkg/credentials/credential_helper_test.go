package credentials

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"testing"
)

func TestPluginDiscovery(t *testing.T) {

	dir, err := ioutil.TempDir("", "plugin")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dir)
	f1, err := ioutil.TempFile(dir, "AWSStaticCredentials")
	f2, err := ioutil.TempFile(dir, "AzureDynamicCredentials")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(f1.Name())
	fmt.Println(f2.Name())

	plugins := GetInstalledPlugins(dir)
	if len(plugins) != 2 {
		t.Errorf("%v!=2", len(plugins))
	}

}
