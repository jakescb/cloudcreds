package credentials

import (
	"net/rpc"

	"github.com/hashicorp/go-plugin"
)

//Credential is the interface that the plugins implement
type Credential interface {
	GetCredentials(input map[string]interface{}) string
}

//CredentialRPC Here is an implementation that talks over RPC
type CredentialRPC struct{ client *rpc.Client }

//GetCredentials return the plugins credentials
func (c *CredentialRPC) GetCredentials(input map[string]interface{}) string {
	var resp string
	err := c.client.Call("Plugin.GetCredentials", input, &resp)
	if err != nil {
		panic(err)
	}
	return resp

}

//CredentialRPCServer is the RPC server that CredentialRPC talks to, conforming to
// the requirements of net/rpc
type CredentialRPCServer struct {
	// This is the real implementation
	Impl Credential
}

//GetCredentials returns the credentials
func (s *CredentialRPCServer) GetCredentials(args map[string]interface{}, resp *string) error {
	*resp = s.Impl.GetCredentials(args)
	return nil
}

//CredentialPlugin The plug in definition
type CredentialPlugin struct {
	// Impl Injection
	Impl Credential
}

//Server int RPC server
func (p *CredentialPlugin) Server(*plugin.MuxBroker) (interface{}, error) {
	return &CredentialRPCServer{Impl: p.Impl}, nil
}

//Client the RPC client
func (CredentialPlugin) Client(b *plugin.MuxBroker, c *rpc.Client) (interface{}, error) {
	return &CredentialRPC{client: c}, nil
}
