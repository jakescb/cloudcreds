package authorization

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"testing"

	"gopkg.in/h2non/gock.v1"
)

func TestStringToken(t *testing.T) {
	st := StringToken{Token: "foo"}
	if fmt.Sprintf("%s", st) != "foo" {
		t.Errorf("%s!=foo", st)
	}

}

func TestFileAuthenticationEngine(t *testing.T) {
	//create a temporary file with a line of text
	tmpFile, err := ioutil.TempFile(os.TempDir(), "cc-*.txt-")
	if err != nil {
		log.Fatal("Cannot create temp file")
	}
	defer os.Remove(tmpFile.Name())

	text := []byte("MyPassword")
	if _, err = tmpFile.Write(text); err != nil {
		log.Fatal("Failed to write to file ", err)
	}
	//now we can finally test it
	fae := FileAuthenticationEngine{FilePath: tmpFile.Name()}
	token, err := fae.GetToken(false)
	if err != nil {
		log.Fatal("Failef to get the token ", err)
	}

	if fmt.Sprintf("%s", token) != "MyPassword" {
		t.Errorf("%s!=MyPassword", token)
	}

}

func TestVaultUsernamePasswordAuthenticationEngine(t *testing.T) {
	vResponse := VaultReturn{Auth: Auth{ClientToken: "MyToken"}}
	vReturn, _ := json.Marshal(vResponse)

	defer gock.Off()

	gock.New("http://myvault.com").Post("auth/userpass/login").Reply(200).JSON(vReturn)

	u := VaultUsernamePasswordAuthenticationEngine{}
	u.VaultAddress = "http://myvault.com"
	u.Username = "Test"
	u.Password = "Test"
	token, err := u.GetToken(false)
	if err != nil {
		log.Fatalf("Error getting token %s", err)
	}

	if fmt.Sprintf("%s", token) != "MyToken" {
		t.Errorf("%s!=MyToken", token)
	}
}

func TestVaultLDAPAuthenticationEngine(t *testing.T) {
	vResponse := VaultReturn{Auth: Auth{ClientToken: "MyToken"}}
	vReturn, _ := json.Marshal(vResponse)

	defer gock.Off()

	gock.New("http://myvault.com").Post("auth/ldap/login").Reply(200).JSON(vReturn)

	u := VaultLDAPAuthenticationEngine{}
	u.VaultAddress = "http://myvault.com"
	u.Username = "Test"
	u.Password = "Test"
	token, err := u.GetToken(false)
	if err != nil {
		log.Fatalf("Error getting token %s", err)
	}

	if fmt.Sprintf("%s", token) != "MyToken" {
		t.Errorf("%s!=MyToken", token)
	}
}
