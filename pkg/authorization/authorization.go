package authorization

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

//Token represents the admission to the resource
type Token interface {
	String() string
}

//StringToken is the simpliest of all tokens
type StringToken struct {
	Token string
}

//String representation of a token
func (st StringToken) String() string {
	return st.Token
}

//AuthenticationEngine purpose is too get tokens
type AuthenticationEngine interface {
	GetToken(saveToDisk bool) (Token, error)
}

//FileAuthenticationEngine the simplest authentication engine, just looks on the file system
type FileAuthenticationEngine struct {
	FilePath string
}

//GetToken gets the token from the specified file path
func (fae *FileAuthenticationEngine) GetToken(saveToDisk bool) (Token, error) {
	token, err := ioutil.ReadFile(fae.FilePath)
	if err != nil {
		return nil, err
	}
	return StringToken{Token: string(token)}, nil
}

//VaultAuthenticationEngine Any use of vault can be wrapped up here
type VaultAuthenticationEngine struct {
	VaultAddress string
	VaultPayload map[string]string
}

//VaultReturn is the structure of a vault login
type VaultReturn struct {
	LeaseID       string      `json:"lease_id"`
	Renewable     bool        `json:"renewable"`
	LeaseDuration int         `json:"lease_duration"`
	Data          interface{} `json:"data"`
	Warnings      interface{} `json:"warnings"`
	Auth          Auth        `json:"auth"`
}

//Metadata is extra data about the login
type Metadata struct {
	Username string `json:"username"`
}

//Auth is the actual authorization piece
type Auth struct {
	ClientToken   string   `json:"client_token"`
	Accessor      string   `json:"accessor"`
	Policies      []string `json:"policies"`
	Metadata      Metadata `json:"metadata"`
	LeaseDuration int      `json:"lease_duration"`
	Renewable     bool     `json:"renewable"`
}

//GetToken gets the token from the vault authentication engine
func (vae *VaultAuthenticationEngine) GetToken(saveToDisk bool) (Token, error) {

	byts, err := json.Marshal(vae.VaultPayload)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", vae.VaultAddress, bytes.NewBuffer(byts))
	if err != nil {
		return nil, err
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var vr VaultReturn
	json.Unmarshal(body, &vr)
	if saveToDisk {
		home, err := os.UserHomeDir()
		if err != nil {
			fmt.Printf("Could not save to disk: %s", err)
		}
		ioutil.WriteFile(home+"/.vault-token", []byte(vr.Auth.ClientToken), os.ModeDevice)
	}
	return StringToken{Token: vr.Auth.ClientToken}, nil
}

//VaultUsernamePasswordAuthenticationEngine simply uses username/password credentials
type VaultUsernamePasswordAuthenticationEngine struct {
	VaultAuthenticationEngine
	Username string
	Password string
}

//GetToken gets the token from the vault authentication engine
func (v *VaultUsernamePasswordAuthenticationEngine) GetToken(saveToDisk bool) (Token, error) {
	url := v.VaultAddress + fmt.Sprintf("/auth/userpass/login/%s", v.Username)
	payload := map[string]string{
		"password": v.Password,
	}
	ve := VaultAuthenticationEngine{VaultAddress: url, VaultPayload: payload}
	return ve.GetToken(saveToDisk)
}

//VaultLDAPAuthenticationEngine simply uses username/password credentials
type VaultLDAPAuthenticationEngine struct {
	VaultAuthenticationEngine
	Username string
	Password string
}

//GetToken gets the token from the vault authentication engine
func (v *VaultLDAPAuthenticationEngine) GetToken(saveToDisk bool) (Token, error) {
	url := v.VaultAddress + fmt.Sprintf("/auth/ldap/login/%s", v.Username)
	payload := map[string]string{
		"password": v.Password,
	}
	ve := VaultAuthenticationEngine{VaultAddress: url, VaultPayload: payload}
	return ve.GetToken(saveToDisk)
}
