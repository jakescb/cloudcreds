package file

import (
	"log"
	"os"
	"path/filepath"
	"strings"
)

//ExpandTilde expands the tilde into the user's home directory
func ExpandTilde(tildePath string) (string, error) {
	//make sure the tilde is at the front
	if strings.HasPrefix(tildePath, "~") {
		home, err := os.UserHomeDir()
		if err != nil {
			return tildePath, err
		}
		actualPath := filepath.Join(home, tildePath[2:])
		return actualPath, nil
	}
	return tildePath, nil
}

func visitFiles(files *[]string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Fatal(err)
		}
		if !info.IsDir() {
			*files = append(*files, path)
		}
		return nil
	}
}

//WalkFiles is the same as filepath.Walk except it does not return directories
func WalkFiles(root string) []string {

	var files []string
	err := filepath.Walk(root, visitFiles(&files))
	if err != nil {
		panic(err)
	}

	return files

}
