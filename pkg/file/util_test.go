package file

import (
	"log"
	"testing"

	homedir "github.com/mitchellh/go-homedir"
)

func TestExpandTilde(t *testing.T) {
	path1 := "~/foo/bar"
	path2 := "/foo/bar"
	path3 := "/foo/~bar"
	path4 := "~/foo/~bar"

	home, err := homedir.Dir()
	if err != nil {
		log.Fatal("Cannot find home dir ", err)
	}

	ans1, err := ExpandTilde(path1)
	if err != nil {
		t.Errorf("Cannot expand %s", path1)
	}
	ans2, err := ExpandTilde(path2)
	if err != nil {
		t.Errorf("Cannot expand %s", path2)
	}
	ans3, err := ExpandTilde(path3)
	if err != nil {
		t.Errorf("Cannot expand %s", path3)
	}
	ans4, err := ExpandTilde(path4)
	if err != nil {
		t.Errorf("Cannot expand %s", path4)
	}

	if ans1 != home+"/foo/bar" {
		t.Errorf("%s!=%s", ans1, home+"/foo/bar")
	}
	if ans2 != "/foo/bar" {
		t.Errorf("%s!=/foo/bar", ans2)
	}
	if ans3 != "/foo/~bar" {
		t.Errorf("%s!=/foo/~bar", ans3)
	}
	if ans4 != home+"/foo/~bar" {
		t.Errorf("%s!=%s", ans1, home+"/foo/~bar")
	}

}
