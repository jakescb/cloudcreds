package provider

//Provider is the bridge between the authentication engines and the credential output
type Provider interface {
	GetCredentialMap() map[string]interface{}
}

//MapProvider is a simple Provider that has two maps it turns to one
type MapProvider struct {
	Common     map[string]interface{}
	Individual map[string]interface{}
}

//GetCredentialMap gets the map for inputs into credentials
func (m *MapProvider) GetCredentialMap() map[string]interface{} {
	results := make(map[string]interface{})
	for k, v := range m.Common {
		results[k] = v
	}

	for k, v := range m.Individual {
		results[k] = v
	}

	return results
}
