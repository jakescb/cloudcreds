package provider

import "testing"

func TestMapProviderNoOverwrites(t *testing.T) {
	p := MapProvider{}

	common := make(map[string]interface{})
	individual := make(map[string]interface{})

	common["common1"] = "data1"
	common["common2"] = "data2"
	individual["individual1"] = "data3"
	individual["individual2"] = "data4"

	p.Common = common
	p.Individual = individual

	r := p.GetCredentialMap()
	if len(r) != 4 {
		t.Error("len(r)!=4")
	}

}

func TestMapProviderOverwrites(t *testing.T) {
	p := MapProvider{}

	common := make(map[string]interface{})
	individual := make(map[string]interface{})

	common["common1"] = "data1"
	common["common2"] = "data2"
	individual["common1"] = "data3"
	individual["individual2"] = "data4"

	p.Common = common
	p.Individual = individual

	r := p.GetCredentialMap()
	if len(r) != 3 {
		t.Error("len(r)!=4")
	}
	if r["common1"] != "data3" {
		t.Errorf("%s!=data3", r["common1"])
	}

}
