module gitlab.com/cloudcreds.io/cloudcreds

go 1.14

require (
	github.com/hashicorp/go-hclog v0.0.0-20180709165350-ff2cf002a8dd
	github.com/hashicorp/go-plugin v1.2.2
	github.com/mitchellh/go-homedir v1.1.0
	gopkg.in/h2non/gock.v1 v1.0.15
)
